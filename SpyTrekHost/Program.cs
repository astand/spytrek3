﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using StreamHandler;
using MessageHandler.Abstract;
using MessageHandler.ConcreteHandlers;
using MessageHandler;
using System.Threading;
using SpyTrekHost.UserUI;
using System.Windows.Forms;
using System.Diagnostics;

namespace SpyTrekHost
{
    class Program
    {

        static TcpClient tcpClient;

        static ListNodesForm listNodes;

        static void Main(string[] args)
        {

            Console.Write($"Please input listening port number : ");
            var PortNumber = Console.ReadLine();
            Console.WriteLine("");
            UInt16 PortNumberNum;
            if (UInt16.TryParse(PortNumber, out PortNumberNum) == false)
                PortNumberNum = 20201;

            Console.WriteLine($"Tcp listener has started @ {DateTime.Now.ToShortTimeString()}.Port number is {PortNumberNum}");

            var tcpListener = new TcpListener(IPAddress.Any, PortNumberNum);
            tcpListener.Start();

            Thread td = new Thread(Dispatcher);

            td.Start();

            listNodes = new ListNodesForm();

            HICollection.AddListUpdater(listNodes.UpdateListNodes);

            Thread ui = new Thread(UIThread);
            ui.Start();

            while (true)
            {
                try
                {
                    tcpClient = tcpListener.AcceptTcpClient();
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Cannot accept client : {e.Message}");
                    continue;
                }

                HICollection.Add(new HandleInstance(tcpClient.GetStream()));
            }
        }


        static void Dispatcher()
        {
            Console.WriteLine($"Dispatcher has started @ {DateTime.Now.ToShortTimeString()}");
        }



        static void UIThread()
        {
            Application.Run(listNodes);
        }
    }
}
